FROM jruby:9.2.7.0-alpine as build

WORKDIR /app

ADD Gemfile Rakefile logstash-input-pulsar.gemspec /app/

RUN bundle install
RUN rake install_jars

COPY lib /app/lib

RUN gem build logstash-input-pulsar.gemspec

FROM docker.elastic.co/logstash/logstash:7.9.3
COPY --from=build /app/logstash-input-pulsar-2.3.0.gem .
RUN bin/logstash-plugin install --no-verify logstash-input-pulsar-2.3.0.gem

